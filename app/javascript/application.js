// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import '@hotwired/turbo-rails'
import 'controllers'
import 'bootstrap'
import 'jquery'
import '@popperjs/core'
import '@rails/activestorage'
import '@rails/actioncable'
import WeatherViewer from 'weather_viewer'

WeatherViewer.start()
