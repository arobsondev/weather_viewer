class GetRecurrenceData {
  constructor() {
    this.elementHumidity = document.getElementById("umidade")
    this.elementTemperature = document.getElementById("temperatura")
    this.runRecurrence()
  }

  runRecurrence() {
    this.getTemperatureRepeatedly()
    this.getHumidityRepeatedly()
  }

  getTemperatureRepeatedly() {
    setInterval(this.getValueOverview("/overview/temperature", this.elementTemperature), 5000)
  }

  getHumidityRepeatedly() {
    setInterval(this.getValueOverview("/overview/humidity", this.elementHumidity), 5000)
  }

  getValueOverview(endpoint, element) {
    var request = new XMLHttpRequest();
  
    request.open("GET", endpoint, true);
    request.send();
    request.onreadystatechange = function() {
      if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
        element.innerHTML = this.responseText;
      }
    }
  }
}

export default GetRecurrenceData
