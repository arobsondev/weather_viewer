import GetRecurrenceData from 'weather_viewer/pages/overview/get_recurrence_data'

export default {
  overview: {
    GetRecurrenceData,
  },
}
