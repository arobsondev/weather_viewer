import pages from 'weather_viewer/pages'

class WeatherViewer {
  start() {
    window.weatherViewer = this.initializeWeatherViewer()
  }

  initializeWeatherViewer() {
    return {
      pages,
    }
  }
}

export default new WeatherViewer()
