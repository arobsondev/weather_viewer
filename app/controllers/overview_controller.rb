class OverviewController < ApplicationController
  before_action :set_temperature, only: %i[index temperature]
  before_action :set_humidiy, only: %i[index humidity]

  def index; end

  def temperature
    render json: @temperature
  end

  def humidity
    render json: @humidity
  end

  private

  def set_temperature
    @temperature = get_from_topic("temperature")
  end

  def set_humidiy
    @humidity = get_from_topic("humidity")
  end

  def get_from_topic(topic)
    index = Rails.cache.keys.select { |key| key.start_with?(topic) }.last
    result = Rails.cache.read(index)

    result[:data]
  end
end
