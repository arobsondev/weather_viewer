HOST      = ENV.fetch("MQTT_HOST")
PORT      = ENV.fetch("MQTT_PORT")
CLIENT_ID = ENV.fetch("MQTT_CLIENT_ID")

mqtt_client = MQTT::Client.connect(host: HOST, port: PORT, client_id: CLIENT_ID)

def save_cache(topic, data)
  key = "#{topic}:#{DateTime.current.to_i}"
  value = { data:, monitored_at: DateTime.current }
  Rails.cache.write(key, value, expires_in: 5.minutes)
end

begin
  Thread.new do
    mqtt_client.get(ENV.fetch("MQTT_TOPIC_TEMPERATURE")) do |topic, message|
      Rails.logger.error("#{topic}: #{message}")
      save_cache(topic, message)
    end

    mqtt_client.get(ENV.fetch("MQTT_TOPIC_HUMIDITY")) do |topic, message|
      Rails.logger.error("#{topic}: #{message}")
      save_cache(topic, message)
    end
  end
rescue
  mqtt_client.disconnect
end
